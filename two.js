//traversing of DOM tree
const rootNode=document.getRootNode();
console.log(rootNode.childNodes);
const htmlElementNode=rootNode.childNodes[0];
console.log(htmlElementNode);
console.log(htmlElementNode.childNodes);

const headElementNode=htmlElementNode.childNodes[0];
const textNode1=htmlElementNode.childNodes[1];
const bodyElementNode= htmlElementNode.childNodes[2];
console.log(textNode1);
//to check parent
console.log(headElementNode.parentNode);
//silbing relation
console.log(headElementNode.nextSibling);
console.log(headElementNode.nextSibling.nextSibling);
console.log(headElementNode.childNodes);

//to change the color of div element
const h1=document.querySelector("h1");
const div=h1.parentNode;
div.style.color="white";
div.style.backgroundColor="grey";

//if we want to reach to body 
const h1=document.querySelector("h1");
const body=h1.parentNode.parentNode;
body.style.color="white";
body.style.backgroundColor="grey";




