//to check how many classes are there in particular part
const sectionTodo=document.querySelector(".section-todo");
console.log(sectionTodo.classList);

//giving class using javascript
sectionTodo.classList.add('bg-dark');

//remove class using javascript
sectionTodo.classList.remove('bg-dark');

///to check whether the class exists or not
const  ans=sectionTodo.classList.contains("container");
console.log(ans);

//toggle class
sectionTodo.classList.toggle("bg-dark");

