//elem.insertAdjacentHTML(where,html)
//beforebegin,afterbegin,beforeend,afterend;

const todoList=document.querySelector(".todo-list");
todoList.insertAdjacentHTML("beforeend","<li>Teach Students</li>");

//clone nodes
const ul=document.querySelector(".todo-list");
const li=document.createElement("li");
li.textContent="new todo";
const li2=li.cloneNode(true); //deep cloning
ul.append(li);
ul.prepend(li2);

