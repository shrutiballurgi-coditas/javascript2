function myFunction(power){
    return function(number){
        return number ** power
    }
}
const square = myFunction(2);
const ans1 = square(3);
console.log(ans1);
const cube = myFunction(3);
const ans2 = cube(3);
console.log(ans2);
function myFunction(power){
    return function(number){
        return number ** power
    }
}
const square2 = myFunction(2);
const ans3 = square2(3);
console.log(ans3);
const cube2 = myFunction(3);
const ans4 = cube2(3);
console.log(ans4);