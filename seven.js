//static list vs live list

//static list:querySelectorAll gives static list
const listItems=document.querySelectorAll(".todo-list li");
const sixthLi=document.createElement("li");
sixthLi.textContent="item 6";
const ul=document.querySelector(".todo-list");
ul.append(sixthLi);
console.log(listItems);

//live list:getSelectorAll gives live list
// const listItems=ul.getElementsByTagName("li");

//how to get dimension of element
const sectionTodo=document.querySelector(".section-todo");
const info=sectionTodo.getBoundingClientRect().width;
console.log(info);






