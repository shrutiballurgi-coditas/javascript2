function func(){
    let counter = 0;
    return function(){
        if(counter < 1){
            console.log("Hi You Called me");
            counter++;
        }else{
            console.log("You Already Called me");
        }
    }
}
const myFunc = func();
myFunc();
