console.log("hello World");
let firstName="Harry";
let lastName="Potter";

const myFunction=function(){
    let var1="First Variable";
    let var2="second Variable";
    console.log(var1);
    console.log(var2);
}

//Function execution Context
let foo="foo";
console.log(foo);
function getFullName(firstName,lastName){
    console.log(arguments);
    let myVar="var inside func";
    console.log(myVar);
    const fullName=firstName+" "+lastName;
    return fullName;
}
const personName=getFullName("Harry","Potter");
console.log(personName);

//lexical environment
// const lastName = "Potter";
// const printName = function(){
//     const firstName = "Harry";
//     function myFunction(){
//         console.log(firstName);
//         console.log(lastName);
//     }
//     myFunction()
// }
// printName();