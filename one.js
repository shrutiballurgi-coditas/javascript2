//DOM-document object model
//select element using get element by id
console.log(document.getElementById("main-heading"));
console.log( typeof document.getElementById("main-heading"));

const mainHeading=document.getElementById("main-heading");
console.log(mainHeading);


//selecting element using get element by id
// const mainHeading=document.querySelector("#main-heading");
const header=document.querySelector(".header");
console.log(header);

const navItem=document.querySelector(".nav-item");
console.log(navItem);

//to select all items in a list
// const navItem=document.querySelectorAll(".nav-item");
// console.log(navItem1);

//change text:textContent and innerText
const mainHeading = document.getElementById("main-heading");
console.log(mainHeading.textContent);
//if we want only innerText
console.log(mainHeading.innerText);
mainHeading.textContent="this is changed content";
console.log(mainHeading.textContent);

//change the styles of elements
const mainHeading=document.querySelector("div");
console.log(mainHeading);

mainHeading.style.color="blue";
//to change backgroundColor
//mainHeading.style.backgroundColor="blue";

//get and set attributes
const link=document.querySelector("a");
console.log(link.getAttribute("href"));

const inputElement=document.querySelector(".form-todo input");
console.log(inputElement.getAttribute("type"));

//set attribute
link.setAttribute("href","https://freeCodeCamp.com");

//get multiple elements using getElements by class name
//get multiple elements items using querySelectorAll
const navItems=document.getElementsByClassName("nav-item");
console.log(navItems[3]);

//loop:simple for loop,for of loop,forEach

for(let i=0;i< navItems.length;i++)
{
    const navItem = navItems[i];
    navItem.style.backgroundColor = "white";
    navItem.style.color="green";
    navItem.style.fontWeight="bold";
}

for(let navItem of navItems){
    navItem.style.backgroundColor = "white";
    navItem.style.color="green";
    navItem.style.fontWeight="bold";

}

navItems=Array.from(navItems);
console.log(Array.isArray(nav));
navItem.forEach((navItem)=>{
    navItem.style.backgroundColor = "white";
    navItem.style.color="green";
    navItem.style.fontWeight="bold";

})

let navItems = document.querySelectorAll("a");
console.log(navItems);

//innerHTML
const headline =document.querySelector(".headline");
console.log(headline.innerHTML);
headline.innerHTML = "<h1>Inner html changed </h>";
headline.innerHTML +="<button class=\"btn\">Learn More </button>"
console.log(headline.innerHTML);
