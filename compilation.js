// compilation ,code execution,closures
console.log(this);
console.log(window);
console.log(firstName);
var firstName = "Harry";
console.log(firstName);

// hoisting
console.log(this);
console.log(window);
console.log(myFunction);
console.log(fullName);
function myFunction(){
    console.log("This is my function");
}
var firstName = "Harry";
var lastName = "Potter"
var fullName = firstName + " " + lastName;
console.log(fullName);


//function
console.log(myFunction);
var myFunction=function myFunction(){
    console.log("This is my Function");
}
consle.log(myFunction);

// Uncaught ReferenceError 
// Cannot access 'firstName' before initialization
console.log(firstName);
let firstName;
console.log(firstName);
console.log(typeof firstName);
firstName = "Harry";